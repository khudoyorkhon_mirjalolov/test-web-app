# quizz-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Admin user
```
phone: 990909765555
```
### Admin user
```
password: admin
```

