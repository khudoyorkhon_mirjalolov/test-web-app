import { createStore } from "vuex";

export default createStore({
  state: () => ({
    testList: JSON.parse(localStorage.getItem('testList')) || [],
    usersList: JSON.parse(localStorage.getItem('usersList')) || [],
    user: JSON.parse(localStorage.getItem('user')) || {},
    isLoggedIn: localStorage.getItem('isLoggedIn') || false,
  }),
  mutations: {
    setNewTest(state, newTest) {
      state.testList.push(newTest);
      localStorage.setItem('testList', JSON.stringify(state.testList));
    },
    removeTest(state, testId) {
      state.testList.splice(
        state.testList.findIndex((item) => item.id === testId),
        1,
      );
      localStorage.setItem('testList', JSON.stringify(state.testList));
    },
    updateTestList(state, updatedTest) {
      state.testList.splice(
        state.testList.findIndex((item) => item.id === updatedTest.id),
        1,
        updatedTest
      );
      localStorage.setItem('testList', JSON.stringify(state.testList));
    },
    removeUser(state, user) {
      state.usersList.splice(
        state.usersList.findIndex((item) => item.pinfl === user.pinfl),
        1,
      );
      localStorage.setItem('usersList', JSON.stringify(state.usersList));
    },
    setNewUser(state, newUser) {
      state.usersList.push(newUser);
      localStorage.setItem('usersList', JSON.stringify(state.usersList));
    },
    setNewUserData(state, updateUserData) {
      state.usersList.splice(
        state.usersList.findIndex((item) => item.id === updateUserData.pinfl),
        1,
        updateUserData
      );
      console.log(state.usersList);
      localStorage.setItem('usersList', JSON.stringify(state.usersList));
    },
    setCurrentUser(state, user) {
      state.user = user;
      state.isLoggedIn = true;
      localStorage.setItem('user', JSON.stringify(state.user));
      localStorage.setItem('isLoggedIn', JSON.stringify(state.isLoggedIn));
    },
  },
  actions: {
    addNewTest(context, payload) {
      context.commit('setNewTest', payload);
    },
    removeFromList(context, payload) {
      context.commit('removeTest', payload)
    },
    updateTest(context, payload) {
      context.commit('updateTestList', payload)
    },
    addNewUser(context, payload) {
      context.commit('setNewUser', payload)
    },
    updateUser(context, payload) {
      context.commit('setNewUserData', payload)
    },
    userLogin(context, payload) {
      context.commit('setCurrentUser', payload)
    },
    deleteUser(context, payload) {
      context.commit('removeUser', payload)
    }
  },
  getters: {
    getTestList(state) {
      return state.testList;
    },
    getTestCount(state) {
      return state.testList.length;
    },
    getTest: (state) => (id) => {
      return state.testList.find((item) => item.id === id)
    },
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
    user(state) {
      return state.user;
    },
    getUser: (state) => (pinfl) => {
      return state.usersList.find((item) => item.pinfl === pinfl)
    },
    getUsers(state) {
      return state.usersList;
    },
    getSortedUsers: (state) => (currentCourse) => {
      if (currentCourse === null) {
        return state.usersList;
      }
      return state.usersList.filter((user) => +user.course === +currentCourse);
    }
  }
});