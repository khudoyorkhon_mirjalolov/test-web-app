
import { createRouter, createWebHistory  } from 'vue-router';
import store from '@/store';
const Workspace = () =>
  import(/* webpackChunkName: "group-user" */ '../views/WorkspaceView.vue')
const routes = [
  {
    path: '/',
    meta: {
      requiresAuth: true,
      permissions: [
        'admin',
        'student'
      ],
    },
    children: [
      {
        path: '',
        name: 'Workspace',
        component: Workspace,
        children: [
          {
            path: 'test-list/questions/',
            meta: {
              permissions: [
                'admin',
              ],
            },
            children: [
              {
                path: '',
                name: 'TestQuestions',
                component: () => import(/* webpackChunkName: "about" */ '@/views/TestQuestions.vue')
              },
              {
                path: 'create',
                name: 'QuestionCreate',
                component: () => import(/* webpackChunkName: "about" */ '@/views/QuestionCreateView.vue')
              },
              {
                path: ':questionId/edit',
                name: 'QuestionEdit',
                component: () => import(/* webpackChunkName: "about" */ '@/views/QuestionEditView.vue')
              },
            ],
          },
          {
            path: 'test-pass',
            meta: {
              permissions: [
                'admin',
                'student'
              ],
            },
            children: [
              {
                path: '',
                name: 'SurverView',
                component: () => import(/* webpackChunkName: "about" */ '@/views/SurverView.vue')
              },
              {
                path: 'finish-test',
                name: 'FinishSurvey',
                meta: {
                  permissions: [
                    'admin',
                    'student'
                  ],
                },
                component: () => import(/* webpackChunkName: "about" */ '@/views/FinishSurvey.vue'),
              },
            ],
          },
          {
            path: 'users-table',
            name: 'UsersTable',
            meta: {
              permissions: [
                'admin',
                'student'
              ],
            },
            component: () => import(/* webpackChunkName: "about" */ '@/views/UsersTableView.vue')
          },
          {
            path: 'user-edit/:pinfl',
            name: 'UserEdit',
            meta: {
              permissions: [
                'admin',
                'student'
              ],
            },
            component: () => import(/* webpackChunkName: "about" */ '@/views/UserEditView.vue')
          }
        ]
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      requiresAuth: false,
      permissions: [],
    },
    component: () => import(/* webpackChunkName: "about" */ '@/views/LoginView.vue')
  },
  {
    path: '/registration',
    name: 'Register',
    meta: {
      requiresAuth: false,
      permissions: [],
    },
    component: () => import(/* webpackChunkName: "about" */ '@/views/RegistrationView.vue')
  },

];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to) => {
  if(to.meta.requiresAuth && !store.getters.isLoggedIn) {
    return {
      path: '/login',
      query: { redirect: to.fullPath },
    }
  } else {
    if (to.meta?.permissions.length > 0) {
      if (!to.meta?.permissions.includes(store.getters.user.userType)) {
        window.location.replace('/');
      }
    }
  }
});

export default router;
